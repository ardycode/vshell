package ru.ardyc.vshell

import org.apache.commons.vfs2.FileObject
import ru.ardyc.vshell.commands.*
import java.io.File
import java.util.Scanner


class UserFileSystem(val fileObject: FileObject) {

    private var currentPath = fileObject

    companion object {
        val ANSI_RESET = "\u001B[0m"
        val ANSI_BLACK = "\u001B[30m"
        val ANSI_RED = "\u001B[31m"
        val ANSI_GREEN = "\u001B[32m"
        val ANSI_YELLOW = "\u001B[33m"
        val ANSI_BLUE = "\u001B[34m"
        val ANSI_PURPLE = "\u001B[35m"
        val ANSI_CYAN = "\u001B[36m"
        val ANSI_WHITE = "\u001B[37m"
    }

    val commands = arrayListOf(LsCommand(), CdCommand(), PwdCommand(), CatCommand(), EchoCommand())

    fun start() {
        var command = ""
        while (!command.equals("exit")) {
            print("${ANSI_GREEN}ardyc@vshell$ANSI_RESET:${currentPath.name.path}\$ ")
            command = readln()
            var isCmd = false
            commands.forEach {
                if (it.isCommand(command)) {
                    isCmd = true
                    currentPath = it.perform(command, currentPath)
                }
            }

            if (!isCmd) {
                println("${command}: command not found")
            }
        }
    }

    fun start(file: File) {
        var command = ""
        if (!file.exists()) {
            println("Файл не существует")
            return
        }
        val scanner = Scanner(file)

        if (!scanner.nextLine().equals("#!/bin/bash")) {
            println("Нет пути до запускаемого файла")
        }
        while (scanner.hasNextLine()) {
            //print("${ANSI_GREEN}ardyc@vshell$ANSI_RESET:${currentPath.name.path}\$ ")
            command = scanner.nextLine()
            commands.forEach {
                if (it.isCommand(command))
                    currentPath = it.perform(command, currentPath)
            }
        }
    }


    fun startDemo() {
        var command = ""
        LsCommand().perform("ls", currentPath)
        currentPath = CdCommand().perform("cd avrora", currentPath)
        LsCommand().perform("ls", currentPath)
        EchoCommand().perform("echo hello", currentPath)
        PwdCommand().perform("pwd", currentPath)
        CatCommand().perform("cat index.html", currentPath)
    }

}