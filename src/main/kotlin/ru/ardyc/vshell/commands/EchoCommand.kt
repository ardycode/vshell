package ru.ardyc.vshell.commands

import org.apache.commons.vfs2.FileObject

class EchoCommand : Command() {

    override fun isCommand(command: String): Boolean {
        return command.startsWith("echo")
    }

    override fun perform(command: String, fileSystem: FileObject): FileObject {
        var string = ""
        val list = command.split(" ")
        for (i in 1 until list.size)
            string += list[i] + " "
        println(string)
        return super.perform(command, fileSystem)
    }
}