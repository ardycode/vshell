package ru.ardyc.vshell.commands

import org.apache.commons.vfs2.FileObject

open class Command {
    open fun isCommand(command: String): Boolean = false
    open fun perform(command: String, fileSystem: FileObject): FileObject {return fileSystem}
}