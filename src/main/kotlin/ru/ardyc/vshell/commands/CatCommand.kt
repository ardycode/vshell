package ru.ardyc.vshell.commands

import org.apache.commons.vfs2.FileObject
import java.io.BufferedReader
import java.io.InputStreamReader

class CatCommand : Command() {

    override fun isCommand(command: String): Boolean {
        return command.startsWith("cat")
    }

    override fun perform(command: String, fileSystem: FileObject): FileObject {
        var args = command.split(" ")
        if (args.size == 1) {
            println("")
            return fileSystem
        }

        var arg = args[1]

        try {
            val resFile = fileSystem.resolveFile(arg)
            if (resFile.isFolder) {
                println("cat: ${resFile.name.baseName}/: Is a directory")
                return fileSystem
            }
            println(BufferedReader(InputStreamReader(resFile.content.inputStream)).readText())
            return fileSystem
        } catch (e: Exception) {
            println("cat: cannot access '${arg}': No such file or directory")
            return fileSystem;
        }

        println("cat: ${args[1]}: No such file or directory")


        return super.perform(command, fileSystem)
    }

}