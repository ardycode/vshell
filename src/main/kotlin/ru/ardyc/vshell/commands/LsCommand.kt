package ru.ardyc.vshell.commands

import ru.ardyc.vshell.UserFileSystem.Companion.ANSI_RED
import ru.ardyc.vshell.UserFileSystem.Companion.ANSI_RESET
import org.apache.commons.vfs2.FileObject

class LsCommand : Command() {
    override fun isCommand(command: String): Boolean {
        return command.startsWith("ls")
    }

    override fun perform(command: String, fileSystem: FileObject): FileObject {
        val args = command.split(" ")
        var isFnd = false
        if (args.size == 1) {
            fileSystem.children.forEach {
                isFnd = true
                if (it.isFolder)
                    println(ANSI_RED + it.name.baseName + ANSI_RESET)
                else println(it.name.baseName)
            }
            if (!isFnd) {
                println("ls: cannot access '${args[1]}': No such file or directory")
            }
            return fileSystem
        } else {
            var arg = args[1]
            var fileObject = fileSystem

            if (arg.startsWith("/")) {
                arg = arg.substring(1, arg.length)
                fileObject = getRootFileObject(fileObject)
            }

            try {
                val resolvedFile = fileObject.resolveFile(arg)
                if (resolvedFile.isFolder) {
                    resolvedFile.children.forEach {
                        isFnd = true
                        if (it.isFolder)
                            println(ANSI_RED + it.name.baseName + ANSI_RESET)
                        else println(it.name.baseName)
                    }
                    if (!isFnd) {
                        println("ls: cannot access '${args[1]}': No such file or directory")
                    }
                    return fileSystem
                }

                println("-bash: ls: ${args[1]}: No such file or directory")
            } catch (e: Exception) {
                println("-bash: ls: ${args[1]}: No such file or directory")
            }
        }
        return fileSystem
    }

    fun getRootFileObject(fileObject: FileObject): FileObject {
        if (fileObject.parent != null && fileObject.name.path != "/")
            return getRootFileObject(fileObject.parent)
        return fileObject
    }
}