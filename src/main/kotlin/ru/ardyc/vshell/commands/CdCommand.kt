package ru.ardyc.vshell.commands

import org.apache.commons.vfs2.FileObject
import kotlin.io.path.name

class CdCommand : Command() {

    override fun isCommand(command: String): Boolean {
        return command.startsWith("cd")
    }

    override fun perform(command: String, fileSystem: FileObject): FileObject {
        val args: List<String> = command.split(" ")
        if (args.size == 1)
            return fileSystem

        var arg = args[1]

        if (arg == ".." && fileSystem.name.path == "/")
            return fileSystem

        var fileObject = fileSystem

        if (arg.startsWith("/")) {
            arg = arg.substring(1, arg.length)
            fileObject = getRootFileObject(fileObject)
        }

        try {
            val resolvedFile = fileObject.resolveFile(arg)
            if (resolvedFile.isReadable)
                return resolvedFile

            println("-bash: cd: ${args[1]}: No such file or directory")
        } catch (e: Exception) {
            println("-bash: cd: ${args[1]}: No such file or directory")
        }


        return fileSystem
    }

    fun getRootFileObject(fileObject: FileObject): FileObject {
        if (fileObject.parent != null && fileObject.name.path != "/")
            return getRootFileObject(fileObject.parent)
        return fileObject
    }
}