package ru.ardyc.vshell.commands

import org.apache.commons.vfs2.FileObject

class PwdCommand: Command() {

    override fun isCommand(command: String): Boolean {
        return command.startsWith("pwd")
    }

    override fun perform(command: String, fileSystem: FileObject): FileObject {
        println(fileSystem.name.path)
        return super.perform(command, fileSystem)
    }

}