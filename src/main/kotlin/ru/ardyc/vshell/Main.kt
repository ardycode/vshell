package ru.ardyc.vshell

import org.apache.commons.vfs2.FileObject
import org.apache.commons.vfs2.FileSystemOptions
import org.apache.commons.vfs2.VFS
import org.apache.commons.vfs2.impl.DefaultFileSystemConfigBuilder
import org.apache.commons.vfs2.provider.zip.ZipFileSystemConfigBuilder
import java.io.File
import java.net.URI
import java.nio.charset.Charset
import java.nio.file.Paths
import java.util.zip.ZipFile
import kotlin.system.exitProcess

fun main(args: Array<String>) {

    if (args.isEmpty()) {
        println("Использование команды: vshell file.zip")
        exitProcess(0)
    }

    val fileName = args[0]
    val fileSystem = openFileSystem(fileName)

    if (args.size == 1) {
        UserFileSystem(fileSystem).start()
        return
    }

    if (args[1] == "--script") {
        if (args.size != 3) {
            println("Используйте --script файл")
            return
        }
        UserFileSystem(fileSystem).start(File(args[2]))
    }

    if (args[1] == "--demo") {
        UserFileSystem(fileSystem).startDemo()
    }


}


fun openFileSystem(path: String): FileObject {
    val fileSystemManager = VFS.getManager()
    var fileSystemOptions = FileSystemOptions()
    ZipFileSystemConfigBuilder.getInstance().setCharset(fileSystemOptions, Charset.forName("Utf-8"))

    return fileSystemManager.resolveFile(
        "zip:${
            Paths.get("").toAbsolutePath()
        }/$path", fileSystemOptions
    )
}

